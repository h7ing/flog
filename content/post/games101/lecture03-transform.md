---
title: Lecture03-Transform 变换
date: 2021-08-15T22:33:00
math: true
---


## Lecture03-Transform 变换

### 缩放

推导过程

结论
$$
\begin{pmatrix}
x^\prime \\ y^\prime
\end{pmatrix}
=
\begin{pmatrix}
S_x&0 \\ 0&S_y
\end{pmatrix}
\begin{pmatrix}
x \\ y
\end{pmatrix}
$$

### 反射(Refelection)

$$
\begin{pmatrix}
x^\prime \\ y^\prime
\end{pmatrix}
=
\begin{pmatrix}
-1&0 \\ 0&1
\end{pmatrix}
\begin{pmatrix}
x \\ y
\end{pmatrix}
$$

<center>按y轴翻转</center>



### 切变(Shear)

$$
\begin{pmatrix}
x^\prime \\ y^\prime
\end{pmatrix}
=
\begin{pmatrix}
1&a \\ 0&1
\end{pmatrix}
\begin{pmatrix}
x \\ y
\end{pmatrix}
$$

<center>水平方向切变</center>


$$
\begin{pmatrix}
x^\prime \\ y^\prime
\end{pmatrix}
=
\begin{pmatrix}
1&0 \\ b&1
\end{pmatrix}
\begin{pmatrix}
x \\ y
\end{pmatrix}
$$

<center>竖直方向切变</center>

### 旋转

$$
\begin{pmatrix}
x^\prime \\ y^\prime
\end{pmatrix}
=
\begin{pmatrix}
\cos{\theta} & -\sin{\theta} \\ 
\sin{\theta} & \cos{\theta}
\end{pmatrix}
\begin{pmatrix}
x \\ y
\end{pmatrix}
$$

补充：旋转矩阵为正交矩阵：$R_\theta ^ {\ -1} = R_\theta ^ {\ T}$
$$
R_{-\theta} = \begin{pmatrix} \cos\theta & \sin\theta \\ -\sin\theta & \cos\theta \end{pmatrix}
= R_{\theta} ^ {\ T}
$$
因为旋转 $-\theta$ 相当于旋转 $\theta$ 的逆操作，即 $R_{-\theta} = R_\theta ^ {\ -1}$

所以得到这个结论
$$
R_\theta ^ {\ -1} = R_\theta ^ {\ T}
$$


### 线性变换

$$
\begin{pmatrix}
x^\prime \\ y^\prime
\end{pmatrix}
=
\begin{pmatrix}
a&b \\ c&d
\end{pmatrix}
\begin{pmatrix}
x \\ y
\end{pmatrix}
$$



### 平移(Translation)

$$
\begin{pmatrix}
x^\prime \\ y^\prime
\end{pmatrix}
=
\begin{pmatrix}
1&0 \\ 0&1
\end{pmatrix}
\begin{pmatrix}
x \\ y
\end{pmatrix}
+
\begin{pmatrix}
t_x \\
t_y
\end{pmatrix}
$$



### 齐次坐标

平移操作引入w坐标系
$$
\begin{pmatrix}
x^\prime \\ 
y^\prime \\
w^\prime
\end{pmatrix}
=
\begin{pmatrix}
1 & 0 & t_x \\ 
0 & 1 & t_y \\
0 & 0 & 1
\end{pmatrix}
\begin{pmatrix}
x \\ y \\ 1
\end{pmatrix}
$$
齐次坐标

当 $w \ne 0$ 时，表示点，如果 $w=1$ ，则表示2D坐标下的点 $P(x,y)$
$$
\begin{pmatrix}
x \\ 
y \\
w
\end{pmatrix}
=
\begin{pmatrix}
x/w \\
y/w \\
1
\end{pmatrix}
$$
当 $w=0$ 时，表示向量

### 仿射变换

仿射变换 = 线性变换 + 平移
$$
\begin{pmatrix}x^\prime \\ y^\prime\end{pmatrix}=\begin{pmatrix}a&b \\ c&d\end{pmatrix}\begin{pmatrix}x \\ y\end{pmatrix}
+ \begin{pmatrix} t_x \\ t_y \end{pmatrix}
$$
用齐次坐标表示：
$$
\begin{pmatrix}x^\prime \\ y^\prime \\ 1\end{pmatrix}=\begin{pmatrix}a&b&t_x \\ c&d&t_y \\ 0&0&1\end{pmatrix}\begin{pmatrix}x \\ y\\1\end{pmatrix}
$$

### 逆变换

逆变换相当于一个变换的反向操作

### 分解变换

变换的组合

先旋转，后平移：
$$
T_{(1,0)} R_{45}
$$




### 3D变换

3D点：$(x, y, z, 1) ^ T$

3D向量：$(x,y,z,0) ^ T$


## Model Transform

将物体坐标转换为世界坐标系

数学上，先缩放，后旋转，再平移：$T \cdot R \cdot S \cdot p$

实际写代码时，则按照顺序从左往右写：平移，旋转，缩放

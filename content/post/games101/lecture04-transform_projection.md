---
date: 2023-05-04T20:55:00
update: 2024-02-11 21:15
title: Transform
math: true
---

## Viewing transformation

### View / Camera transformation

约定：相机始终处于原点，look at -z，向上方向为y轴

物体和相机做相同变换，使相机变换到标准位置

先平移到原点，再旋转：旋转可先计算x，y，z的旋转，再求逆旋转即转置矩阵


也可以用坐标系变换：参考虎书7.5 Coordinate Transformations

相机坐标系：`p`点为世界坐标系中的原点坐标，`uvw`分别对应正交的三轴，`w`为相机指向的反方向

推导过程（2D）：先旋转后平移到相机原点

相机坐标系转换到标准坐标系：
$$
p_{xy} = 
\begin{vmatrix}x_p\\y_p\\1\end{vmatrix} =
\begin{vmatrix}1&0&x_e\\0&1&y_e\\0&0&1\end{vmatrix}
\begin{vmatrix}x_u&x_v&0\\y_u&y_v&0\\0&0&1\end{vmatrix}
\begin{vmatrix}u_p\\v_p\\1\end{vmatrix}
$$
推导过程：

假设相机坐标系为`uv` ，仅旋转后转换到标准坐标系 `xy` ：

$$
xy = \begin{vmatrix}
a&b\\
c&d
\end{vmatrix} \begin{vmatrix} u \\ v \end{vmatrix}
$$

可得：

$$
\begin{gather}
au+bv = x \\
cu+dv = y
\end{gather}
$$

其中：

`(0,1)` 对应 $(x_u,y_u)$ ，`(1,0)` 对应 $(x_v,y_v)$ ，这两个点是 `uv` 轴单位向量在 `xy` 坐标系中的坐标

代入方程可计算出旋转变换矩阵。

而转换到相机坐标系为上述变换的逆变换，平移操作可以取负得到逆变换，旋转操作可直接转置矩阵（因为该旋转矩阵默认为归一化的正交矩阵，矩阵的逆等于转置矩阵）：

$$
p_{uv} = 
\begin{vmatrix}x_u&y_u&0\\x_v&y_v&0\\0&0&1\end{vmatrix}
\begin{vmatrix}1&0&-x_e\\0&1&-y_e\\0&0&1\end{vmatrix}
\begin{vmatrix}x_p\\y_p\\1\end{vmatrix}
$$


上述变换即为相机变换（将标准坐标系`oxyz`转换到相机坐标系`puvw`）：

$$
M_{view} = 
\begin{vmatrix}u & v & w & e\end{vmatrix}^{-1} = 
\begin{vmatrix}x_u&y_u&z_u&0\\x_v&y_v&z_v&0\\x_w&y_w&z_w&0\\0&0&0&1\end{vmatrix}
\begin{vmatrix}1&0&0&-x_e\\0&1&0&-y_e\\0&0&1&-z_e\\0&0&0&1\end{vmatrix}
$$

`uvw` 的计算：虎书 8.1.3 The Camera Transformation

定义 `t` 为竖直方向向量（与 `w` 和 `u` 正交），`g` 为相机朝向的方向，`w` 为相机朝向的反方向，可得另外两个轴（右手系）：

$$
\begin{align}
w &= -\frac{g}{\vert g \vert} \\
u &= \frac{t \times w}{\vert t \times w \vert} \\
v &= w \times u
\end{align}
$$
`t` 简单起见使用 `(0,1,0)` 即可，

`g` 的推导：假设相机位于原点，其旋转值分别为 (pitch, yaw, roll) (分别对应绕xyz轴旋转)

想象一个由单位方向向量构成对角线的立方体，则：

$$
\begin{align}
y &= \sin({pitch}) \\
x &= \cos({pitch}) \cos({yaw}) \\
z &= \cos({pitch}) \sin({yaw}) 
\end{align}
$$

更直观的，将旋转命名为 $(R_x, R_y, R_z)$ 则：

$$
\begin{align}
g_x &= \cos R_x \cos R_y \\
g_y &= \sin R_x \\
g_z &= \cos R_x \sin R_y

\end{align}
$$


将相机变换改写为`Look`函数：

```c
#include <cglm/cglm.h>

void Look(vec3 campos, vec3 direction, vec3 camup, mat4 dest) {
	vec3 w;
    glm_vec3_negate_to(direction, w); // w = -g (direction should be normlized)
    vec3 u;
    glm_vec3_crossn(camup, w, u); // u = t x w (u is normalized inside)
    vec3 v;
    glm_vec3_cross(w, u, v); // v = w x u

    glm_mat4_identity(dest);
    glm_translate(dest, (vec3){-campos[0],-campos[1],-campos[2]});
    
    // glm按列保存矩阵
    glm_mat4_mul((mat4){
        {u[0],v[0],w[0],0},
        {u[1],v[1],w[1],0},
        {u[2],v[2],w[2],0},
        {0,0,0,1.0f}}, dest, dest);
}
```



### Projection transformation

正交投影：先平移到原点，再缩放到canonical 立方体（缩放到-1～1）

透视投影：远平面挤压成近平面大小，再做正交投影


正交投影变换矩阵（左手系）：

$$
M_{orth} = \begin{vmatrix}
\frac{2}{r-l} & 0 & 0 & -\frac{r+l}{r-l}\\
0 & \frac{2}{t-b} & 0 & -\frac{t+b}{t-b}\\
0 & 0 & \frac{2}{n-f} & -\frac{n+f}{n-f}\\
0 & 0 & 0 & 1
\end{vmatrix}
$$


右手系：
$$
M_{OpenGL} = \begin{vmatrix}
\frac{2}{r-l} & 0 & 0 & -\frac{r+l}{r-l}\\
0 & \frac{2}{t-b} & 0 & -\frac{t+b}{t-b}\\
0 & 0 & \frac{-2}{f-n} & -\frac{n+f}{f-n}\\
0 & 0 & 0 & 1
\end{vmatrix}
$$


todo: 公式推导：使用fov转换


### 透视变换的推导

先约定：近平面$z=n$，变换后不变

则，远平面$z=f$，$x$ 和 $y$，在变换后的值，可通过相似三角形计算得出：
$x^\prime = \frac{n}{z}x$

用齐次坐标表示变换后的坐标：
$\begin{bmatrix}x'&y'&z'\end{bmatrix}^T$

根据齐次坐标特性，乘系数w=z后可得：
$\begin{bmatrix}nx&ny&?&z\end{bmatrix}^T$

当$z=n$时，$z'$ 仍 $=n$，则乘$n$后可得$n^2$：
$\begin{bmatrix}nx&ny&n^2&n\end{bmatrix}^T$

可反推得到第三行z的矩阵为：
$\begin{bmatrix}0&0&A&B\end{bmatrix}$

即 $An+B=n^2$

而远平面则取平面中心点 $0,0,z,1$ 即 $0,0,f,1$
这个点在变换后仍为中心点不变，即 $0,0,f,1$

则齐次坐标中$w=f$时，该点矩阵为
$\begin{bmatrix}0&0&f^2&f\end{bmatrix}$

可反推得到变换矩阵为：
$Af+B=f^2$

解方程组可得
$A=n+f$
$B=-nf$

最后得到透视变换到正交的矩阵

$$
\begin{vmatrix}
n & 0 & 0 & 0\\
0 & n & 0 & 0\\
0 & 0 & n+f & -nf\\
0 & 0 & 1 & 0
\end{vmatrix}
$$

最最后再与正交变换矩阵点乘，可得透视变换矩阵：

$$
M_{per} = \begin{vmatrix}
\frac{2n}{r-l} & 0 & \frac{l+r}{l-r} & 0\\
0 & \frac{2n}{t-b} & \frac{b+t}{b-t} & 0\\
0 & 0 & \frac{f+n}{n-f} & \frac{2fn}{f-n}\\
0 & 0 & 1 & 0
\end{vmatrix}
$$

右手系：

$$
M_{OpenGL} = 
$$


## 参考

```
title: GAMES101投影矩阵推导详解和分析
url: https://blog.csdn.net/n5/article/details/122535066?spm=1001.2014.3001.5501
```


```
[OpenGL Projection Matrix (songho.ca)](http://www.songho.ca/opengl/gl_projectionmatrix.html)
```
---
id: 202305301102
date: 2023-05-30T11:02:00
title: Shading
math: true
---

画家算法

z-buffer：深度缓冲

frame buffer
depth buffer 深度缓存

z-buffer算法
复杂度：O(n)，若有n个三角形：因为没有对三角形排序，只是逐像素求最小值
与三角形的计算顺序无关
浮点数精度问题，一般不会出现完全相同的深度值，相同值的情况没在这里讨论

着色
blinn-phong反射模型

光源：specular高光，diffuse漫反射，ambient环境光（间接光照）

局部
着色不含阴影

### 漫反射

diffuse reflection

shading point
法线与光线方向的夹角决定了亮度
light falloff

lambertian (diffuse) shading 余弦


```
// 参考虎书 5.2.1 Lambertian reflection

float diffk = max(dot(norm, lightDir), 0);
vec3 diffuse = diffk * lightColor;
FragColor = vec4(diffuse * objectColor, 1.0);
```


```
// 参考LearnOpenGL Basic Lighting

关于 normal matrix
// 在虎书 17.3.1 中有提到

当三角形不规则缩放时，直接缩放法线向量并不一定能与表面正交，此时需要用另一个normal matrix来计算缩放后的法线向量：

normal matrix 为 去除了平移的模型变换矩阵的逆的转置矩阵

// shader实现:
mat3 norm = mat3(transpose(inverse(model)));
vec3 Normal = norm * aNormal;

// cpu实现:
// ... calc the model matrix
// mat4 mat;
// mat4 matinv;
// mat3 norm;
glm_mat4_inv(transform->mat, transform->matinv); // 求mat4的逆
glm_mat4_pick3t(transform->matinv, transform->norm); // mat4转置保存为mat3

```

$$
normal\ matrix = (M^{-1})^T
$$

## 高光

半程向量与法线接近
该模型中，指数一般用100-200

```
// 参考虎书 5.2.2 Specular reflection

vec3 lightDir = normalize(lightPos - fragPos);
vec3 camDir = normalize(camPos - fragPos);
vec3 halfvec = normalize(lightDir + camDir); // 半程向量
float shininess = 128.0f;
vec3 specular = pow(max(0, dot(norm, halfvec)), shininess);
```

```
// 参考虎书 17.13.1 Blinn-Phong Shader Program
// 在view space中计算光照


```

```
// 参考LearnOpenGL Basic Lighting

float shininess = 100.0f;
vec3 eyeDir = normalize(eyePos - fragPos);
vec3 lightDir = normalize(lightPos - fragPos);
vec3 reflectDir = reflect(-lightDir, norm); // 反射函数光线方向为光源指向被照点
// 视线与反射光方向的夹角
vec3 specular = pow(max(0, dot(eyeDir, reflectDir)), shininess);
```


$视线与反射光方向夹角 = 2 \times 半程向量与法线夹角$


推导过程：

假设视线 $e$ 与光线方向 $l$ 的半程向量 $h$ 与法线夹角为 $\beta$ ，视线与反射光方向夹角为 $\alpha$ ，光线与法线夹角为 $\theta$ ，反射光与半程向量的夹角为 $\gamma$ ，则


$$
\begin{align}
\alpha + \gamma &= \beta + \theta \\
\theta &= \beta + \gamma
\end{align}
$$
可得：

$$
\alpha = 2\beta
$$


### 环境光

ambient

参考虎书 5.3 Ambient illumination

环境光可简化为一系列常量系数（每个颜色通道可以不同）

```glsl
float amk = 0.1;
vec3 ambient = amk * lightColor;
FragColor = vec4(ambient * objectColor, 1.0);
```


着色频率

几种不同频率的模型
flat shading: 逐面

gouraud shading: per vertex

phong shading: per pixel


per vertex 逐顶点法线
取周围几个法线的加权平均
todo: 公式 = 周围三角形的法线之和/法线之和的长度

per pixel 法线

shader
phong模型漫反射的shader程序

texture纹理

parameterization

tiled texture

wang tiling


## lecture 9

重心坐标 Barycentric coordinates

texel  纹理元素，纹素

bilinear interpolation 双线性插值

linear interpolation 线性插值
lerp

bicubic interpolation 

mipmap
是原始纹理的4/3

trilinear interpolation

anisotropic filtering 各向异性过滤
ripmap
是原始纹理的3倍大

ewa filtering

application of texturing mapping

environment map

environment lighting

spherical environment map

cube map

## bump mapping

纹理信息：扰动法线

计算：

法线：切线向量旋转90度后归一化

公式：

$$
n = (-dp/du, -dp/dv, 1).normalize
$$

注意：局部坐标系 s,t,n

## displacement mapping

位移贴图：改变顶点位置

代价：模型面数需要比较多，导致需要的采样率比较高
directx: 动态texel

3d纹理
噪声函数

provide precomputed shading
环境光遮挡纹理图

volume rendering 体积渲染
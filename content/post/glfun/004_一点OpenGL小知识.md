---
id: 202312171158
date: 2023-12-17T11:58:24
title: 一点OpenGL小知识
---


## `OpenGL Objects`

```
[OpenGL Object - OpenGL Wiki (khronos.org)](https://www.khronos.org/opengl/wiki/OpenGL_Object)

An **OpenGL Object** is an OpenGL construct that contains some state. When they are bound to the context, the state that they contain is mapped into the context's state. Thus, changes to context state will be stored in this object, and functions that act on this context state will use the state stored in the object.

OpenGL is defined as a "state machine". The various API calls change the OpenGL state, query some part of that state, or cause OpenGL to use its current state to render something.

Objects are always containers for state. Each particular kind of object is defined by the particular state that it contains. An OpenGL object is a way to encapsulate a particular group of state and change all of it in one function call.
```


## `vbo`

`vertex buffer object`

保存顶点数值的对象


用法

```
float vertices[] = {0,0,0, 0,0,0, 0,0,0};

GLuint vbo;
glGenBuffers(1, &vbo);
glBindBuffer(GL_ARRAY_BUFFER, vbo);
glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0); // 设置shader中location=0的输入位置为0的变量的数据在buffer中的布局
glEnableVertexAttribArray(0); // 启用该位置变量布局

glBindBuffer(GL_ARRAY_BUFFER, 0); // 取消绑定
```

## `vao`

`vertex array object`

但实际并不是如名字提示的那样，而是可以理解为 geometry object，因为vao实际上并不是保存vertex的数组，而是保存了对顶点数据属性的设置

或者理解为保存批量`vbo`顶点属性设置的对象

```
https://stackoverflow.com/questions/11821336/


https://computergraphics.stackexchange.com/questions/10332/

[Objects in OpenGL](https://www.khronos.org/opengl/wiki/OpenGL_Object) don't store calls; they store _state_. They're like structs; they have data members. Binding an object to the OpenGL context allows OpenGL operations that access certain state to find that state within the bound object. So if you call a function that modifies certain state that is from a bound object, that function will modify the state within the object.

When you bind a VAO, you are binding an object that stores the state used for vertex arrays. When you call a function that modifies the state for vertex arrays, that function will modify the state stored in the VAO currently bound. When you render, the rendering system will use the vertex array state found in the currently bound VAO.

```


用法

```
GLuint vao;
glGenVertexArrays(1, &vao);
glBindVertexArray(vao);

// ...
glBindVertexArray(0); // 取消绑定
```

## `ebo`

`element buffer object`

保存顶点索引的对象

## `GLSL`

`vertex shader` 的输入参数成为 `vertex attribute`

通常情况下，`OpenGL` 至少支持16个 `vec4` 输入，可通过 `GL_MAX_VERTEX_ATTRIBS` 查询：

```c
GLint n;
glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &n);
```


## `vecN` 的一些基本操作

```glsl
// these operation calls swizzling

vec2 v2;
vec4 v4 = v2.xyxx;
vec3 v3 = v4.zyw;
vec4 v42 = v2.xxxx + v3.yxzy;

vec2 vect2 = vec2(0.5, 0.7);
vec4 vect4 = vec4(vect2, 0, 0);
vec4 result4 = vec4(vect4.xyz, 1.0);
```